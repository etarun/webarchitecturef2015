require 'rails_helper'

RSpec.describe "vote_comments/index", type: :view do
  before(:each) do
    assign(:vote_comments, [
      VoteComment.create!(
        :user_id => 1,
        :comment_id => 2
      ),
      VoteComment.create!(
        :user_id => 1,
        :comment_id => 2
      )
    ])
  end

  it "renders a list of vote_comments" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
